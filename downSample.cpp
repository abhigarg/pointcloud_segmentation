#include "stdafx.h"
#include "common.h"



// downsample using clustering unit normal vectors and finding representative normal vector out of each cluster and the corresponding xyz point from the point cloud

int downSample(int K, int minSz, int maxSz, double clustTol, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_xyz_in, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_Vxyz_in, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_xyz_out, 
			   pcl::PointCloud <pcl::Normal>::Ptr &normals_out)
{
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
	tree->setInputCloud (cloud_Vxyz_in);

	// form clusters of unit normal vectors
	vector<pcl::PointIndices> cluster_indices;
	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
	ec.setClusterTolerance (clustTol); // 1 unit ~ 1 rad. each cluster has normal vectors varying by <= 1 radians.  // clustTol = 0.02
	ec.setMinClusterSize (minSz);  //minSz = 30;
	ec.setMaxClusterSize (maxSz);  //maxSz = 100000;
	ec.setSearchMethod (tree);
	ec.setInputCloud(cloud_Vxyz_in);  // apply on unit normal vectors
	ec.extract (cluster_indices);  // indices for each group

	cout << "no of clusters formed: " << cluster_indices.size() << endl;

	// find representative for each cluster using radius search
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_downsample_Vxyz (new pcl::PointCloud<pcl::PointXYZ>);
	int clust_id = 0;
//#ifndef _OPENMP	
//#pragma omp parallel for
	for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
	{
		//extract clusters one by one
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster_Vxyz (new pcl::PointCloud<pcl::PointXYZ>), cloud_cluster_xyz (new pcl::PointCloud<pcl::PointXYZ>);
		for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
		{
			cloud_cluster_Vxyz->points.push_back (cloud_Vxyz_in->points[*pit]); //* extract normal vectors for a cluster one by one
			cloud_cluster_xyz->points.push_back (cloud_xyz_in->points[*pit]);
		}

		cloud_cluster_Vxyz->width = cloud_cluster_Vxyz->points.size ();
		cloud_cluster_Vxyz->height = 1;
		cloud_cluster_Vxyz->is_dense = true;

		cloud_cluster_xyz->width = cloud_cluster_xyz->points.size ();
		cloud_cluster_xyz->height = 1;
		cloud_cluster_xyz->is_dense = true;

		cout << "size of cluster - " << clust_id << " is: " << cloud_cluster_xyz->points.size() << endl;

		// apply radius search on the extracted cluster points one by one and find number of neighbours for each point
		vector<int> pointIdxNKNSearch(K);
		vector<float> pointNKNSquaredDistance(K);

		pcl::KdTreeFLANN<pcl::PointXYZ> kdtree2;

		int id = 0;
		while(id < cloud_cluster_xyz->points.size())
		{
			// find arbitrary point from the cluster to begin downsampling
			pcl::PointXYZ searchPoint = cloud_cluster_xyz->points[id];

			// add search point to downsample output cloud for xyz points
			cloud_xyz_out->points.push_back(searchPoint);

			// find unit normal vector for the corresponding search point
			pcl::PointXYZ pt_normal = cloud_cluster_Vxyz->points[id];

			// add unit normal vector to the downsample output cloud for normal vectors
			pcl::Normal pt;
			pt.normal_x = pt_normal.x;
			pt.normal_y = pt_normal.y;
			pt.normal_z = pt_normal.z;
			normals_out->push_back(pt);

			if(cloud_cluster_xyz->points.size() <= K)
				break;

			// temporary clouds to store downsampled points only
			pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_xyz_temp (new pcl::PointCloud<pcl::PointXYZ>);
			pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_Vxyz_temp (new pcl::PointCloud<pcl::PointXYZ>);

			// kd tree to find neighbours of the search point
			kdtree2.setInputCloud(cloud_cluster_xyz);
			
			// remove neighbours from cluster to apply downsampling
			if(kdtree2.nearestKSearch(searchPoint, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0 )
			{
				 pcl::ExtractIndices<pcl::PointXYZ> extract_xyz, extract_Vxyz;
				 pcl::PointIndices::Ptr	pid (new pcl::PointIndices ());
				 //cout << "size of neighbours: " << pointIdxNKNSearch.size() << endl;
				 for(int i = 0; i < pointIdxNKNSearch.size(); i++)
					pid->indices.push_back(pointIdxNKNSearch[i]); // to remove the nearest K neighbours of the current point from the point cloud cluster

				 pid->indices.push_back(id);  // remove also the current point from the point cloud cluster

				 // remove points from xyz point cloud
				 extract_xyz.setInputCloud(cloud_cluster_xyz);
				 extract_xyz.setIndices(pid);
				 extract_xyz.setNegative(true);
				 extract_xyz.filter(*cloud_xyz_temp);

				 // remove points from Vxyz point cloud
				 extract_Vxyz.setInputCloud(cloud_cluster_Vxyz);
				 extract_Vxyz.setIndices(pid);
				 extract_Vxyz.setNegative(true);
				 extract_Vxyz.filter(*cloud_Vxyz_temp);
			}

			// replace cluster cloud with the downsampled temporary cluster
			pcl::copyPointCloud(*cloud_xyz_temp, *cloud_cluster_xyz);
			pcl::copyPointCloud(*cloud_Vxyz_temp, *cloud_cluster_Vxyz);

			id++;
			//cout << "id: " << id << endl;
		}  // for all points in each cluster one by one
	
		//cout << "size of cluster- " << clust_id << " after downsampling: " << cloud_cluster_xyz->points.size() << endl;
		clust_id++;
	} // for all clusters one by one
//#endif
	cloud_xyz_out->width = cloud_xyz_out->points.size();
	cloud_xyz_out->height = 1;
	cloud_xyz_out->is_dense = true;

	return 1;
}
