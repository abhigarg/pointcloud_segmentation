#include "stdafx.h"
#include "common.h"

int fastmesh_gp3(pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals, string fname)
{
	// Initialize objects
	pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
	pcl::PolygonMesh triangles;

	// Create search tree*
	pcl::search::KdTree<pcl::PointNormal>::Ptr tree (new pcl::search::KdTree<pcl::PointNormal>);
	tree->setInputCloud (cloud_with_normals);

	// Set the maximum distance between connected points (maximum edge length)
	gp3.setSearchRadius (5);

	// Set typical values for the parameters
	gp3.setMu (2.5);
	gp3.setMaximumNearestNeighbors (1000);
	gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
	gp3.setMinimumAngle(M_PI/18); // 10 degrees
	gp3.setMaximumAngle(2*M_PI/3); // 120 degrees
	gp3.setNormalConsistency(true);

	// Get result
	gp3.setInputCloud (cloud_with_normals);
	gp3.setSearchMethod (tree);
	gp3.reconstruct (triangles);

	// mesh features:
#ifdef PRINT_COUT
	cout << "No. of faces in triangulated mesh: " << triangles.polygons.size() << endl;
#endif
#ifdef WRITE_FILE
	// save mesh to file
	pcl::io::saveOBJFile(fname,triangles); 
#endif
	return 1;
}