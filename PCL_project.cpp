// PCL_project.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "common.h"


int main()
{
	string data_dir = "C:\\Users\\abgg\\Downloads\\elance\\new\\High_performance_star\\ExtractSurfaces01\\";
	
	// x, y, z coordinates file
	string xyz_file = data_dir;
	xyz_file.append("points_xyz.pcd");

	// Normal components data file
	string Vxyz_file = data_dir;
	Vxyz_file.append("points_Vxyz.pcd");


	// read point clouds from pcd files
	pcl::PCDReader reader;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_xyz (new pcl::PointCloud<pcl::PointXYZ>), cloud_Vxyz (new pcl::PointCloud<pcl::PointXYZ>);
	reader.read (xyz_file, *cloud_xyz); // read x, y, z coordinates from coordinates file and save to memory in PointXYZ cloud form
	reader.read (Vxyz_file, *cloud_Vxyz); // read normal vector components from normal data file and save to memory in PointXYZ cloud form
#ifdef PRINT_OUT	
	cout << "XYZ point cloud before filtering has: " << cloud_xyz->points.size () << " data points." << std::endl;
	cout << "Vxyz before filtering has: " << cloud_Vxyz->points.size () << " data points." << std::endl; 
#endif

	// downsample or not	
	bool is_downsample = true;

#ifndef DOWN_SAMPLE
	is_downsample = false;
#endif
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud <pcl::Normal>::Ptr normals (new pcl::PointCloud <pcl::Normal>);
	
	if(is_downsample)
	{
#ifdef PRINT_COUT
		cout << "applying downsampling ... " << endl;
#endif
		double clustTol = 0.002;
		int minSz = 3;
		int maxSz = 100000;
		int K = 3;

		clock_t t = clock();
		
		downSample(K, minSz, maxSz, clustTol, cloud_xyz, cloud_Vxyz, cloud, normals);

		t = clock() - t;
#ifdef PRINT_TIME
		printf ("Time taken for downsampling: %f seconds\n",((float)t)/CLOCKS_PER_SEC);
#endif
#ifdef PRINT_COUT
		cout << "XYZ points before downsampling: " << cloud_xyz->points.size() << endl;
		cout << "XYZ point cloud has " << cloud->points.size() << " data points after downsampling" << endl;
		cout << "Normals cloud has " << normals->points.size() << " unit vectors after downsampling" << endl;
#endif
	}
	else
	{
		// assign normal vectors read from pcd file to PCL normal form
		for(int i = 0; i < cloud_Vxyz->size(); i++)
		{
			pcl::Normal pt;
			pt.normal_x = cloud_Vxyz->points.at(i).x;
			pt.normal_y = cloud_Vxyz->points.at(i).y;
			pt.normal_z = cloud_Vxyz->points.at(i).z;

			normals->push_back(pt);
		}
	
		// copy all xyz points to cloud for processing
		pcl::copyPointCloud<pcl::PointXYZ, pcl::PointXYZ>(*cloud_xyz, *cloud);
	}
#ifdef PRINT_COUT
	cout << "no of xyz points in cloud: " << cloud->points.size() << endl;
	cout << "no. of normal vectors: " << normals->points.size() << endl;
#endif

#ifdef PLANAR_SEG

	//planar segmentation
	int max_iter = 1000;
	double thresh = 15.0;
	planar_segmentation(cloud, normals, thresh, max_iter);
<<<<<<< HEAD

#endif

#ifdef PLANAR_SEG_FROM_NORMALS

	//planar segmentation from normals
	cout << "apply planar segmentation from normals..." << endl;
	int max_iter = 1000;
	double thresh = 15.0;
	double eps_angle = 5*M_PI/180;
	planar_segmentation_from_normals(cloud, normals, thresh, eps_angle, max_iter);

#endif

#ifdef RGN_GROW_SEG

	// region_growing_segmentation
	pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;

	int minClustSz = 8000;      // min. size of cluster or region to be a surface
	int maxClustSz = 80000;     // max. points on a surface
	float smoothness_thresh = 3;  // upto 3 degrees of deviation between normal vectors in a neighbourhood is allowed
	float curvature_thresh = 2;  // tolerance for change in curvature as region grows

	region_growing_segmentation(cloud, normals, reg, minClustSz, maxClustSz, smoothness_thresh, curvature_thresh);

#endif

	return 1;
}



=======

#endif

#ifdef PLANAR_SEG_FROM_NORMALS

	//planar segmentation from normals
	cout << "apply planar segmentation from normals..." << endl;
	int max_iter = 1000;
	double thresh = 15.0;
	double eps_angle = 5*M_PI/180;
	planar_segmentation_from_normals(cloud, normals, thresh, eps_angle, max_iter);

#endif

#ifdef RGN_GROW_SEG

	// region_growing_segmentation
	pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
	region_growing_segmentation(cloud, normals, reg, minClustSz, maxClustSz, smoothness_thresh, curvature_thresh);

#endif

	return 1;
}



>>>>>>> b04fff43039461b20208a921e3c2af51837ebd26








	/*
	string xyz_file = data_dir;
	xyz_file.append("points_xyz.pcd");

	string Vxyz_file = data_dir;
	Vxyz_file.append("points_Vxyz.pcd");


	// read point clouds from pcd files

	pcl::PCDReader reader;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_xyz (new pcl::PointCloud<pcl::PointXYZ>), cloud_Vxyz (new pcl::PointCloud<pcl::PointXYZ>);
	reader.read (xyz_file, *cloud_xyz);
	cout << "XYZ point cloud before filtering has: " << cloud_xyz->points.size () << " data points." << std::endl;

	reader.read (Vxyz_file, *cloud_Vxyz);
	cout << "Vxyz before filtering has: " << cloud_Vxyz->points.size () << " data points." << std::endl; 

	
	bool is_downsample = false;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud <pcl::Normal>::Ptr normals (new pcl::PointCloud <pcl::Normal>);
	
	if(is_downsample)
	{
		cout << "applying downsampling ... " << endl;

		double clustTol = 0.002;
		int minSz = 3;
		int maxSz = 100000;
		int K = 3;

		clock_t t = clock();
		
		downSample(K, minSz, maxSz, clustTol, cloud_xyz, cloud_Vxyz, cloud, normals);

		t = clock() - t;

		printf ("Time taken for downsampling: %f seconds\n",((float)t)/CLOCKS_PER_SEC);
		cout << "XYZ points before downsampling: " << cloud_xyz->points.size() << endl;
		cout << "XYZ point cloud has " << cloud->points.size() << " data points after downsampling" << endl;
		cout << "Normals cloud has " << normals->points.size() << " unit vectors after downsampling" << endl;
	}
	else
	{
		// assign normal vectors read from pcd file to PCL normal form
	
		for(int i = 0; i < cloud_Vxyz->size(); i++)
		{
			pcl::Normal pt;
			pt.normal_x = cloud_Vxyz->points.at(i).x;
			pt.normal_y = cloud_Vxyz->points.at(i).y;
			pt.normal_z = cloud_Vxyz->points.at(i).z;

			normals->push_back(pt);
		}
	
		// copy all xyz points to cloud for processing
		pcl::copyPointCloud<pcl::PointXYZ, pcl::PointXYZ>(*cloud_xyz, *cloud);
	}

	cout << "no of xyz points in cloud: " << cloud->points.size() << endl;
	cout << "no. of normal vectors: " << normals->points.size() << endl;

	//pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_xyzrgb (new pcl::PointCloud<pcl::PointXYZRGB>);

	//pcl::copyPointCloud<pcl::PointXYZ, pcl::PointXYZRGB>(*cloud_xyz, *cloud_xyzrgb);


	// don segmentation

	
	// convert to ply
	//pcl::PLYWriter ply_write;
	//ply_write.write("normals_xyz.ply", *normals, true);

	
	//planar segmentation
	int max_iter = 1000;
	double thresh = 15.0;
	//double eps_angle = 15*M_PI/180;
	planar_segmentation(cloud, normals, thresh, max_iter);
	

	

	//planar segmentation from normals
	cout << "apply planar segmentation from normals..." << endl;
	int max_iter = 1000;
	double thresh = 15.0;
	double eps_angle = 5*M_PI/180;
	planar_segmentation_from_normals(cloud, normals, thresh, eps_angle, max_iter);
	
	
	
	cout << "applying region growing based segmentation ... " << endl;

	int minClustSz = atoi(argv[1]);
	int maxClustSz = atoi(argv[2]);
	float smoothness_thresh = (float)atof(argv[3]);
	float curvature_thresh = (float)atof(argv[4]);

	// region_growing_segmentation
	pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
	clock_t t = clock();
	region_growing_segmentation(cloud, normals, reg, minClustSz, maxClustSz, smoothness_thresh, curvature_thresh);
	t = clock() - t;

	printf ("Time taken for region growing segmentation: %f seconds\n",((float)t)/CLOCKS_PER_SEC);
	
	


	return 1;
}


*/



/*
	cout << "assigning colour to point cloud" << endl;
	
	
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_xyzrgb (new pcl::PointCloud<pcl::PointXYZRGB>);

	
	uint8_t r = 255;
	uint8_t g = 0;
	uint8_t b = 0;
	int32_t rgb_clr = (r << 16) | (g << 8) | b;
	

	//cloud_xyzrgb->points.resize(cloud_xyz->size());
	//pcl::copyPointCloud<pcl::PointXYZ, pcl::PointXYZRGB>(*cloud_xyz, *cloud_xyzrgb);
	
	for (size_t i = 0; i < cloud_xyz->points.size(); i++) 
	{
		cloud_xyzrgb->points[i].x = cloud_xyz->points[i].x;
		cloud_xyzrgb->points[i].y = cloud_xyz->points[i].y;
		cloud_xyzrgb->points[i].z = cloud_xyz->points[i].z;

		//cloud_xyzrgb->points[i].rgb = *(float *)(&rgb);   // makes all points red
	}
	
	// write red coloured points to pcd file
	cout << "write red coloured points to pcd file" << endl;
	pcl::PCDWriter writer;
	writer.write<pcl::PointXYZRGB> ("points_xyz_red.pcd", *cloud_xyzrgb, true);
	

	// visualize coloured points and normals
	cout << "visualize coloured points and normals" << endl;
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
	viewer->initCameraParameters ();
	viewer->addCoordinateSystem (1.0);
	
	int v1(0);
	viewer->createViewPort(0.0, 0.0, 0.5, 1.0, v1);
	viewer->setBackgroundColor (0, 0, 0, v1);
	viewer->addText("Radius: 0.01", 10, 10, "v1 text", v1);
	pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud_xyzrgb);
	viewer->addPointCloud<pcl::PointXYZRGB> (cloud_xyzrgb, rgb, "colored cloud1", v1);
	
	int v2(0);
	viewer->createViewPort (0.0, 0.0, 1.0, 1.0, v2);
	viewer->setBackgroundColor (0.3, 0.3, 0.3, v2);
	viewer->addText("Radius: 0.01", 10, 10, "v2 text", v2);
	
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> single_color(cloud_xyzrgb, 0, 255, 0);
	
	viewer->addPointCloud<pcl::PointXYZRGB> (cloud_xyzrgb, single_color, "colored cloud2", v2);
	viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "colored cloud2");
	viewer->addPointCloudNormals<pcl::PointXYZRGB, pcl::Normal> (cloud_xyzrgb, normals, 10, 0.05, "normals", v2);
	
	while (!viewer->wasStopped ())
	{
		viewer->spinOnce (100);
		boost::this_thread::sleep (boost::posix_time::microseconds (100000));
	}




	// visualize normals
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
	viewer->initCameraParameters ();
	viewer->setBackgroundColor (0, 0, 0);
	viewer->addCoordinateSystem (1.0);

	//create viewports and adding point clouds
	int v1(0);
	viewer->createViewPort (0.0, 0.0, 1.0, 1.0, v1);
	viewer->createViewPortCamera(v1);
	viewer->addText("original point cloud", 10, 10, "v1 text", v1);
	//viewer->addPointCloud<pcl::PointXYZ>(cloud_xyz, "original point cloud", v1);
	viewer->addPointCloudNormals<pcl::PointXYZ, pcl::Normal>(cloud_xyz, normals, 100, 0.01999F, "Point cloud and normals", v1);
	//viewer.addPointCloudNormals<pcl::PointXYZ,pcl::Normal>(cloud_xyz, normals, v1);
	
	while (!viewer->wasStopped ())
	{
		viewer->spinOnce (100);
		boost::this_thread::sleep (boost::posix_time::microseconds (100000));
	}


	
	// visualize normals
    pcl::visualization::PCLVisualizer viewer("PCL Viewer");
    viewer.setBackgroundColor (0.0, 0.0, 0.5);
    viewer.addPointCloudNormals<pcl::PointXYZ,pcl::Normal>(cloud_xyz, normals);

    while (!viewer.wasStopped ())
    {
		viewer.spinOnce ();
    }
	*/

	// cluster extraction
	//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
	//cluster_extraction(cloud_xyz, cloud_filtered);


	// region_growing_segmentation
	//pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
	//region_growing_segmentation(cloud_xyz, normals, reg);

	/*
	// visualize
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
	viewer->initCameraParameters ();
	viewer->setBackgroundColor (0, 0, 0);
	viewer->addCoordinateSystem (1.0);

	//create viewports and adding point clouds
	int v1(0);
	viewer->createViewPort (0.0, 0.0, 1.0, 1.0, v1);
	viewer->createViewPortCamera(v1);
	viewer->addText("original point cloud", 10, 10, "v1 text", v1);
	viewer->addPointCloud<pcl::PointXYZ>(cloud_xyz, "original point cloud", v1);
	//viewer->addPolygonMesh(surfaceMesh, "original mesh", v1);

	
	int v2(0);
	viewer->createViewPort (0.33, 0.0, 0.67, 1.0, v2);
	viewer->createViewPortCamera(v2);
	viewer->addText("Normals", 10, 10, "v2 text", v2);
	viewer->addPointCloud<pcl::PointXYZ>(cloud_Vxyz, "Normals", v2);
	//viewer->addPolygonMesh(noisy_surfaceMesh, "noisy mesh", v2);

	
	int v3(0);
	viewer->createViewPort (0.67, 0.0, 1.0, 1.0, v3);
	viewer->createViewPortCamera(v3);
	viewer->addText("Filered", 10, 10, "v3 text", v2);
	viewer->addPointCloud<pcl::PointXYZ>(cloud_filtered, "Filtered", v2);
	
	while (!viewer->wasStopped ())
	{
		viewer->spinOnce (100);
		boost::this_thread::sleep (boost::posix_time::microseconds (100000));
	}
	*/