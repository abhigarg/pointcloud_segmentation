#include "stdafx.h"
#include "common.h"


int planar_segmentation(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud <pcl::Normal>::Ptr normals, double thresh, int max_iter)
{
	#ifdef PRINT_COUT
	cout << "apply planar segmentation ... " << endl;
#endif
	// Objects for storing the point clouds.

	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>), cloud_p (new pcl::PointCloud<pcl::PointXYZ>), cloud_f (new pcl::PointCloud<pcl::PointXYZ>);

	// for storing normal vectors
	pcl::PointCloud <pcl::Normal>::Ptr normals_filtered (new pcl::PointCloud <pcl::Normal>), normals_p (new pcl::PointCloud <pcl::Normal>), normals_f (new pcl::PointCloud <pcl::Normal>);

	// make a copy of input point cloud for xyz coordinates and normal vectors
	pcl::copyPointCloud<pcl::PointXYZ, pcl::PointXYZ>(*cloud, *cloud_filtered);
	pcl::copyPointCloud<pcl::Normal, pcl::Normal>(*normals, *normals_filtered);

	//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr inlierPoints(new pcl::PointCloud<pcl::PointXYZ>);
 
	// Object for storing the plane model coefficients.
	pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);

	pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());

	//start clock
	clock_t t = clock();

	// Create the segmentation object.
	pcl::SACSegmentation<pcl::PointXYZ> seg;
	seg.setInputCloud(cloud);
	// Configure the object to look for a plane.
	seg.setModelType(pcl::SACMODEL_PLANE);
	// Use RANSAC method.
	seg.setMethodType(pcl::SAC_RANSAC);
	// set max. iterations
	seg.setMaxIterations (max_iter);    // default 1000
	// Set the maximum allowed distance to the model.
	seg.setDistanceThreshold(thresh);   // default 15.0
	// Enable model coefficient refinement (optional).
	seg.setOptimizeCoefficients(true);

	 // Create the filtering object
	pcl::ExtractIndices<pcl::PointXYZ> extract;
	
	// Object to extract normal vectors
	pcl::ExtractIndices<pcl::Normal> extract_normals;

	int i = 0, nr_points = (int) cloud->points.size ();

	pcl::PLYWriter writer;
	
	// vectors to store point cloud and normals for each surface identified
	vector<pcl::PointCloud<pcl::PointXYZ>> planar_surfaces_clouds;
	vector<pcl::PointCloud<pcl::Normal>> planar_surfaces_normals;
	
	while (cloud_filtered->points.size () > 0.1 * nr_points)
	{
		// Segment the largest planar component from the remaining cloud
		seg.setInputCloud (cloud_filtered);
		seg.segment (*inliers, *coefficients);
		if (inliers->indices.size () == 0)
		{
#ifdef PRINT_COUT
			std::cerr << "Could not estimate a planar model for the given dataset." << std::endl;
#endif
			break;
		}
		
		// Extract the inliers for xyz coordinates
		extract.setInputCloud (cloud_filtered);
		extract.setIndices (inliers);
		extract.setNegative (false);
		extract.filter (*cloud_p);
#ifdef PRINT_COUT
		cout << "Points extracted in surface-" << i << ": " << cloud_p->points.size() << endl;
#endif
		planar_surfaces_clouds.push_back(*cloud_p);

		// Extract the inliers for normal vectors
		extract_normals.setInputCloud (normals_filtered);
		extract_normals.setIndices (inliers);
		extract_normals.setNegative (false);
		extract_normals.filter (*normals_p);

		planar_surfaces_normals.push_back(*normals_p);

		// filter segmented points from original point cloud
		extract.setNegative (true);
		extract.filter (*cloud_f);
		cloud_filtered.swap (cloud_f);

		// filter segmented normals from original normals
		extract_normals.setNegative (true);
		extract_normals.filter (*normals_f);
		normals_filtered.swap (normals_f);

		i++;
	}
	
	t = clock() - t;
	int num_planes = planar_surfaces_clouds.size();
#ifdef PRINT_TIME
	printf ("Time taken for extracting %d planar surfaces containing %f percent of total points: %f seconds\n", num_planes, (1 - (double)cloud_filtered->points.size()/(double)nr_points)*100, 
		   ((float)t)/CLOCKS_PER_SEC);
#endif
#ifdef PRINT_COUT
	cout << "Total number of planar surfacs extracted: " << num_planes << endl;

	cout << "size of remaining points: " << cloud_filtered->points.size() << endl;
#endif
	// Saving extracted surfaces to file and obtaining mesh for each extracted surface	

	for(int i = 0; i < planar_surfaces_clouds.size(); i++)
	{
		// extract surface coordinates and normals
		pcl::PointCloud<pcl::PointXYZ> cloud_i = planar_surfaces_clouds[i];
		pcl::PointCloud<pcl::Normal> normals_i = planar_surfaces_normals[i];

#ifdef PRINT_COUT
		cout << "no of points in surface - " << i << ": " << cloud_i.points.size() << endl;
#endif
		// apply random colour to the extracted point cloud
		pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_xyzrgb (new pcl::PointCloud<pcl::PointXYZRGB>);

		uint8_t r = rand()%255;
		uint8_t g = rand()%255;
		uint8_t b = rand()%255;
		//int32_t rgb_clr = (r << 16) | (g << 8) | b;
	
		cloud_xyzrgb->points.resize(cloud_i.size());
		pcl::copyPointCloud<pcl::PointXYZ, pcl::PointXYZRGB>(cloud_i, *cloud_xyzrgb);

		for (int j = 0; j < cloud_xyzrgb->points.size(); j++) 
		{
			cloud_xyzrgb->points[j].r = r;
			cloud_xyzrgb->points[j].g = g;
			cloud_xyzrgb->points[j].b = b;
		}
#ifdef WRITE_FILE		
		cout << "write surface-" << i << " cloud and normals to file" << endl;

		//write cloud and normals for the surface
		stringstream ss;
		ss << "colored_points_plane_" << num_planes << "_" << i << ".ply";
		writer.write<pcl::PointXYZRGB> (ss.str (), *cloud_xyzrgb, true);	

		stringstream ss_n;
		ss_n << "normals_plane_" << num_planes << "_" << i << ".ply";
		writer.write<pcl::Normal> (ss_n.str (), normals_i, true);
#endif

#ifdef PRINT_COUT
		cout << "concatenating point cloud and normals ..." << endl;
#endif
		// concatenate colored xyz coordinates and normal vectors to single point cloud
		pcl::PointCloud<pcl::PointNormal>::Ptr p_n_cloud (new pcl::PointCloud<pcl::PointNormal>);
		pcl::concatenateFields(cloud_i, normals_i, *p_n_cloud);
#ifdef PRINT_COUT
		cout << "computing mesh for surface using greedy fast mesh algorithm: " << i << endl;
#endif
		// use fast mesh from unorganized points to generate mesh for surface
		stringstream ss_m;
		ss_m << "fast_mesh_plane_" << num_planes << "_" << i << ".obj";
		t = clock();
		fastmesh_gp3(p_n_cloud, ss_m.str());
		t = clock() - t;
#ifdef PRINT_TIME
		printf ("Time taken for generating mesh from point cloud-%d containing %d points: %f seconds\n", i, cloud_i.points.size(), ((float)t)/CLOCKS_PER_SEC);
#endif
		// use Poissons algorithm to generate mesh for surface
#ifdef PRINT_COUT
		cout << "computing mesh for surface using Poissons algorithm: " << i << endl;
#endif
		stringstream ss_ps;
		ss_ps << "poissons_mesh_plane_" << num_planes << "_" << i << ".obj";
		t = clock();
		poisson_mesh(p_n_cloud, ss_ps.str());
		t = clock() - t;
#ifdef PRINT_TIME
		printf ("Time taken for generating mesh from point cloud-%d containing %d points: %f seconds\n", i, cloud_i.points.size(), ((float)t)/CLOCKS_PER_SEC);
#endif
	}

	return 1;
}