// openmp
#include <omp.h>

// General libs
#include <iostream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/thread/thread.hpp>

// pcl -- common
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>

// region_growing_segmentation
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/region_growing.h>

// pcl -- cluster_extraction
#include <pcl/filters/filter.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

// pcl - ply - obj
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/obj_io.h>


// pcl - make mesh from pointcloud
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/console/time.h>
#include <pcl/surface/gp3.h>
#include <pcl/surface/poisson.h>
#include <pcl/surface/marching_cubes_hoppe.h>
#include <pcl/surface/marching_cubes_rbf.h>
#include <pcl/common/common.h>


// pcl -- visualization
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>

// igraph
#include <cstdio>
#include <igraph.h>


// clock
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include <math.h>       /* sqrt */

#ifdef _DEBUG
  #define _SCL_SECURE_NO_WARNINGS
#endif


#define PRINT_COUT
#define WRITE_FILE
#define PRINT_TIME
#define PLANAR_SEG
#define PLANAR_SEG_FROM_NORMALS
#define RGN_GROW_SEG
#define DOWN_SAMPLE

using namespace std;


// weights
double getWeight(pcl::PointXYZ v1, pcl::PointXYZ v2)
{
	double w;

	Eigen::Vector3f p1 = v1.getVector3fMap();
	Eigen::Vector3f p2 = v2.getVector3fMap();

	w = sqrt((p1.x - p2.x)^2 + (p1.y - p2.y)^2 + (p1.z - p2.z)^2);

	return w;
}



// functions

int downSample(int K, int minSz, int maxSz, double clustTol, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_xyz_in, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_Vxyz_in, 
			   pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_xyz_out, pcl::PointCloud <pcl::Normal>::Ptr &normals_out);

int fastmesh_gp3(pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals, string fname);
void poisson_mesh(pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals, string fname);

int region_growing_segmentation(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud <pcl::Normal>::Ptr normals, 
								pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> &reg, int minClustSz, int maxClustSz, float smoothness_thresh, float curvature_thresh);

int planar_segmentation(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud <pcl::Normal>::Ptr normals, double thresh, int max_iter);

int planar_segmentation_from_normals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud <pcl::Normal>::Ptr normals, double thresh, double eps_angle, int max_iter);

// centrality measure
void igraph_centr_measure(pcl::PolygonMesh mesh);











//struct input_parameters{
//	string xyz_fname;
//	string Vxyz_fname;
//
//	bool is_downsample;
//	double clust_tol;
//	int minSz;
//	int maxSz;
//	int K;
//
//	bool is_planar_seg;
//	int max_iter;
//	double dist_thresh;
//	bool optim_coeff;
//	double num_pts;
//
//	bool is_region_growing_seg;
//	int num_nghbr;
//	double smoothness_thresh;
//	double curvature_thresh;
//};