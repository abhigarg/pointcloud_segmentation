#include "stdafx.h"
#include "common.h"

int region_growing_segmentation(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud <pcl::Normal>::Ptr normals, 
								pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> &reg, int minClustSz, int maxClustSz, float smoothness_thresh, float curvature_thresh)
{
	pcl::search::KdTree<pcl::PointXYZ>::Ptr kdtree(new pcl::search::KdTree<pcl::PointXYZ>);
	pcl::PLYWriter writer;
	
	// min. no of points for a cluster or surface to be considered, 
	// if points are less than the min. required then that cluster will not be treated as a cluster
	reg.setMinClusterSize (minClustSz);  

	// max. no of points allowed in a cluster ... 
	// any cluster or surface with more points are not grown anymore
	reg.setMaxClusterSize (maxClustSz);  
	reg.setSearchMethod (kdtree);

	// no of immediate neighbours of the seed point which are considered for growing the segment
	reg.setNumberOfNeighbours (30);    
	
	reg.setInputCloud (cloud);
	reg.setInputNormals (normals);

	// angle between normals at neighbouring points, if this is less than the threshold then the cluster is grown in the direction of neighboring point
	reg.setSmoothnessThreshold (smoothness_thresh / 180.0 * M_PI);   // 3 is a normal value

	// to maintain the curvature of the surface below a certain threshold
	// if the curvature is found to be exceeding the threshold then the surface is not grown in that direction
	reg.setCurvatureThreshold (curvature_thresh);                    

	vector <pcl::PointIndices> cluster_indices; // to store cluster indices
	
	clock_t t = clock();
	reg.extract (cluster_indices);  // perform region growing
	t = clock() - t;

#ifdef PRINT_TIME
	printf ("Time taken for extracting %d surfaces: %f seconds\n", cluster_indices.size(), ((float)t)/CLOCKS_PER_SEC);
#endif

	// to color the regions or clusters or surfaces extracted
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr colored_cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
	colored_cloud = reg.getColoredCloud();

	int numCluster = (int)cluster_indices.size();

#ifdef PRINT_COUT
	cout << "Number of clusters is equal to " << numCluster << endl;
	cout << "First cluster has " << cluster_indices[0].indices.size () << " points." << endl;
#endif
	int j = 0;
	for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
	{
		// extract point cloud for each cluster
		pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZRGB>);

		for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
			cloud_cluster->points.push_back (colored_cloud->points[*pit]); //*
		cloud_cluster->width = (int)cloud_cluster->points.size();
		cloud_cluster->height = 1;
		cloud_cluster->is_dense = true;

#ifdef PRINT_COUT
		cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << endl;
#endif
#ifdef WRITE_FILE
		stringstream ss;
		ss << "rgs_cloud_cluster_" << numCluster << "_theta_" << smoothness_thresh << "_min_" << minClustSz << "_max_" << maxClustSz << "_clust" <<  j << ".ply";
		writer.write<pcl::PointXYZRGB> (ss.str (), *cloud_cluster, true); 
#endif
		j++;
	}	


	return (0);
}