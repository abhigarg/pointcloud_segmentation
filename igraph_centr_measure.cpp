#include "stdafx.h"
#include "common.h"

void igraph_centr_measure(pcl::PolygonMesh mesh)
{
	// get coordinates from mesh
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
	fromPCLPointCloud2(mesh.cloud, *cloud); 

	// number of faces in mesh
	int num_faces = mesh.polygons.size();

	//mesh properties for checking
#ifdef PRINT_COUT
	cout << "no of faces in mesh: " << num_faces << endl;
	cout << "no of vertices in mesh: " << mesh.cloud.data.size() << endl;

	cout << "initialize empty graph... " << endl;
#endif
	// empty graph -- initialize
	igraph_t g_mesh, g_mesh_no_attrib;  //g_mesh is graph with weights added as attributes to edges, g_mesh_no_attrib is graph without any weights attributes
	igraph_empty(&g_mesh, 0, 0);  // initialize graph as empty and undirected
	igraph_empty(&g_mesh_no_attrib, 0, 0); // switch last 0 with 1 to initialize an empty directed graph

	igraph_vector_t edge_weights;  // vector to store weights of each edge

#ifdef PRINT_COUT
	cout << "fill graph with edges from polygon mesh and weight for each edge" << endl;
#endif
	int j = 0; // to iterate edges

	// fill graph with edges from polygon mesh
	for(int i = 0; i < num_faces; i++)
	{
		int vi_1 = mesh.polygons[i].vertices[0];
		int vi_2 = mesh.polygons[i].vertices[1];
		int vi_3 = mesh.polygons[i].vertices[2];

		pcl::PointXYZ v1, v2, v3;

		v1 = cloud->at(vi_1);
		v2 = cloud->at(vi_2);
		v3 = cloud->at(vi_3);

		igraph_add_edge(&g_mesh, vi_1, vi_2);
		igraph_cattribute_EAN_set(&g_mesh, "weight", j, getWeight(v1, v2));

		igraph_add_edge(&g_mesh_no_attrib, vi_1, vi_2);
		VECTOR(edge_weights)[j] = getWeight(v1, v2);
		j++;
		igraph_add_edge(&g_mesh, vi_1, vi_3);
		igraph_add_edge(&g_mesh_no_attrib, vi_1, vi_3);
		igraph_cattribute_EAN_set(&g_mesh, "weight", j, getWeight(v1, v3));
		VECTOR(edge_weights)[j] = getWeight(v1, v3);
		j++;
		igraph_add_edge(&g_mesh, vi_3, vi_2);
		igraph_add_edge(&g_mesh_no_attrib, vi_3, vi_2);
		igraph_cattribute_EAN_set(&g_mesh, "weight", j, getWeight(v3, v2));
		VECTOR(edge_weights)[j] = getWeight(v3, v2);
		j++;
	}
#ifdef PRINT_COUT
	cout << "graph properties: " << endl;
	// show graph properties:
	cout << "vertices in graph: " << igraph_vcount(&g_mesh);
	cout << "edges in graph: " << igraph_ecount(&g_mesh);

	cout << "computing closeness centrality ..." << endl;
#endif
	
	// compute closeness centrality for all vertices
	igraph_vector_t closenessRes1, closenessRes2;   // to store closness centrality for all vertices
	igraph_vector_init(&closenessRes1, 0);  //initialize empty vector
	igraph_vector_init(&closenessRes2, 0);

	igraph_closeness(&g_mesh, &closenessRes1, igraph_vss_all(), IGRAPH_ALL, 0, 1);  // compute closeness centrality for all vertices with graph and edge weights attributes


	igraph_closeness(&g_mesh_no_attrib, &closenessRes2, igraph_vss_all(), IGRAPH_ALL, &edge_weights, 1); // compute weighted closeness centrality

#ifdef PRINT_COUT
	cout << "closeness centre with weights as attribute: " << igraph_vector_max(&closenessRes1) << endl;  // get max. closeness centrality

	cout << "weighted closeness centre measure: " << igraph_vector_max(&closenessRes2) << endl;
#endif

}